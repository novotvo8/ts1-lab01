package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculatorTest {

    @Test
    public void add_2and3_5() {
        //arrange
        Calculator c = new Calculator();
        //act
        int res = c.add(2,3);
        //assert
        assertEquals(5,res);
    }

    @ParameterizedTest(name = "add({0},{1}) = {2}")
    @CsvFileSource(resources = "/input.csv", numLinesToSkip = 1)
    public void additionTest(int a, int b, int c) {
        //arrange
        Calculator cal = new Calculator();
        //act
        int res = cal.add(a,b);
        //assert
        assertEquals(c,res);
    }


    @ParameterizedTest
    @CsvSource(value = {"1,2,3"})
    public void additionTest2(int a, int b, int c) {
        //arrange
        Calculator cal = new Calculator();
        //act
        int res = cal.add(a,b);
        //assert
        assertEquals(c,res);
    }

    private static Stream<Arguments> providerMethod() {
        return Stream.of(
                Arguments.of(1,2,3),
                Arguments.of(3,2,5)
        );
    }

    @ParameterizedTest
    @MethodSource(value = "providerMethod")
    public void additionTest3(int a, int b, int c) {
        //arrange
        Calculator cal = new Calculator();
        //act
        int res = cal.add(a,b);
        //assert
        assertEquals(c,res);
    }

    @ParameterizedTest
    @ValueSource(ints = {2,-4,8,10})
    public void isEvenTest_positive(int a) {
        //arrange
        Calculator c = new Calculator();
        //act
        boolean res = c.isEven(a);
        //assert
        assertTrue(res);
    }


    /*
    @ParameterizedTest(name="add({0}, {1}) = {2}")
    @CsvSource({"1,2,3","2,3,5"})
    public void additionTest(int a, int b, int c) {
        //arrange
        Calculator cal = new Calculator();
        //act
        int res = cal.add(a,b);
        //assert
        assertEquals(c,res);
    }

    @ParameterizedTest(name="add({0}, {1}) = {2}")
    @CsvFileSource(resources = "/input.csv", delimiter = ',', numLinesToSkip = 1)
    public void additionTest2(int a, int b, int c) {
        //arrange
        Calculator cal = new Calculator();
        //act
        int res = cal.add(a,b);
        //assert
        assertEquals(c,res);
    }

    private static Stream<Arguments> providerMethod() {
        return Stream.of(
                Arguments.of(1,2,3),
                Arguments.of(1,2,3)
        );
    }

    @ParameterizedTest
    @MethodSource("providerMethod")
    public void additionTest3(int a, int b, int c) {
        //arrange
        Calculator cal = new Calculator();
        //act
        int res = cal.add(a,b);
        //assert
        assertEquals(c,res);
    }

    @ParameterizedTest(name="isEven({0}) = true")
    @ValueSource(ints = {2,4,0,-2})
    public void isEvenTest(int a) {
        //arrange
        Calculator cal = new Calculator();
        //act
        boolean res = cal.isEven(a);
        //assert
        assertTrue(res);
    }
    */
}
