package cz.cvut.fel.ts1;

public class Calculator {
    public int add(int a, int b) {
        System.out.print(a+b);
        return a + b;
    }

    public boolean isEven(int a) {
        return a%2 == 0;
    }
}
